#!/bin/bash
cd "$(dirname "${BASH_SOURCE[0]}")"
LIST=".nosend"
[ -f ${LIST} ] || touch ${LIST}
shopt -s expand_aliases
SLEEP="${2:-2}"
USAGE="--send | --dry <duration>"

function check_command() { [ -x "$(command -v ${1})" ] || { echo -e "\e[31m${1} not found, please install it.\e[0m" ; return 1 ; } }

if ! check_command telegram-send; then
	if ! check_command bash; then
		exit 1
	fi
	exit 1
fi

if [ "$#" -lt 1 ]; then
        echo "usage: `basename ${0}` ${USAGE}";
        exit 1;
fi

case ${1} in
        --dry) alias telegram-send="echo" ; sleep_time="0" ; cp ${LIST} ${LIST}_dry ; LIST="${LIST}_dry" ; true ;;
	--send) alias telegram-send="telegram-send --audio" ; true ;;
	--help) echo "usage: `basename ${0}` ${USAGE}" ; exit 0 ;;
	*) echo "unknown argument: ${1}" ; exit 1 ;;
esac


#MAIN
for SONG in stream/*.mp3; do
	SHA1SUM=$(sha1sum "${SONG}" |  awk '{ print $1 }')
        if ! grep -Fxq "${SHA1SUM}" ${LIST}; then
		sleep ${SLEEP}
	        telegram-send "${SONG}" 1>/dev/null && { echo "${SHA1SUM}" >> ${LIST} ; echo -e "\e[37m${SONG}\e[0m \e[92mis sent\e[0m" ; }
        else
		echo -e "\e[37m${SONG}\e[0m \e[91mis already sent\e[0m"
        fi
done
